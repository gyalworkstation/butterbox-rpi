set -e

# Install dendrite and keanu-weblite

# Install kubo ipfs
# This is also available in "64-bit ARM", but the "ARM" version works fine.
wget -O /tmp/kubo.tar.gz https://dist.ipfs.tech/kubo/v0.20.0/kubo_v0.20.0_linux-arm.tar.gz
cd /tmp
tar -xvzf kubo.tar.gz
./kubo/install.sh

# make sure the binary can be run by the pi user
chown -R pi:pi /usr/local/bin/ipfs
ipfs --version

# lowpower is an option for hardware like the rpi
# see: https://docs.ipfs.tech/install/command-line/#system-requirements
# in case it's already running, shut it down first
# try to shutdown ipfs, but if it exits 1, that's ok
set +e
ipfs shutdown
set -e

ipfs init --profile=lowpower
chown -R pi:pi /home/pi/.ipfs

# get daemon to start on boot
cp /tmp/butter-setup/configs/butterbox-ipfs.service /lib/systemd/system/ipfs.service
systemctl enable ipfs.service

# Note: this is insecure.  It exposes the whole of the IPFS daemon to anyone
# who is able to access this page, including making config changes.
ipfs config --json API.HTTPHeaders.Access-Control-Allow-Origin "[\"http://$butter_name.lan\"]"
ipfs config --json API.HTTPHeaders.Access-Control-Allow-Methods '["GET", "POST"]'
ipfs config --json API.HTTPHeaders.Access-Control-Allow-Headers '["Authorization"]'
ipfs config --json API.HTTPHeaders.Access-Control-Expose-Headers '["Location"]'
ipfs config --json API.HTTPHeaders.Access-Control-Allow-Credentials '["true"]'
# Have IPFS daemon listen to more than localhost.
ipfs config Addresses.API /ip4/0.0.0.0/tcp/5001
ipfs config Addresses.Gateway /ip4/0.0.0.0/tcp/8080
chown -R pi:pi /home/pi/.ipfs
systemctl start ipfs.service