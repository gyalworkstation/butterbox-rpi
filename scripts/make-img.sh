# Makes an .img of this install and puts it on a USB drive, if inserted.
# Then shrinks that down so it can be copied over
# bbb = butter box backup
set -e

# eliminate some caches
apt-get clean
rm -r /usr/local/go
rm -r /var/lib/apt/lists/*

# Assumes that the usb-butter system has mounted the drive in the expected location
dd if=/dev/mmcblk0 of=/media/usb-butter/bbb.img bs=16M status=progress

# Install PiShrink so we don't end up with a 32GB image
wget https://raw.githubusercontent.com/Drewsif/PiShrink/master/pishrink.sh
chmod +x pishrink.sh
mv pishrink.sh /usr/local/bin

# Shrink the image
pishrink.sh /media/usb-butter/bbb.img