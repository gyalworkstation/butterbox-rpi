# Helpful for local development or debugging troublesome builds
# Runs the install script, tails the logs and shows htop all at once.

sudo apt-get install tmux -y

tmux new-session \; \
  send-keys 'sleep 2 && tail -f /var/log/butter/install.txt' C-m \; \
  split-window -v \; \
  send-keys 'sudo -E bash /tmp/butter-setup/scripts/buttermeup.sh -l en' C-m \; \
  split-window -h \; \
  send-keys 'htop' C-m \; 
