set -e

apt-get install libmicrohttpd-dev -y
cd ~/
git clone https://github.com/nodogsplash/nodogsplash.git
cd nodogsplash
make
make install

# Configure nodogsplash portal, redirect, gateway
# The captive page is built and localized in the butter site.
cp /tmp/butter-site/_site/captive.html /etc/nodogsplash/htdocs
cp -r /tmp/butter-site/_site/assets /etc/nodogsplash/htdocs
cat /etc/nodogsplash/nodogsplash.conf | grep -v ^GatewayInterface | tee /etc/nodogsplash/nodogsplash.conf
echo "GatewayInterface wlan0" | tee -a /etc/nodogsplash/nodogsplash.conf
echo "SplashPage captive.html" | tee -a /etc/nodogsplash/nodogsplash.conf
echo "RedirectURL http://$butter_name.lan" | tee -a /etc/nodogsplash/nodogsplash.conf
echo "GatewayAddress 10.3.141.1" | tee -a /etc/nodogsplash/nodogsplash.conf

# Turn on captive portal
cp ~/nodogsplash/debian/nodogsplash.service /lib/systemd/system/
systemctl enable nodogsplash.service
systemctl start nodogsplash.service